FROM openjdk:17
EXPOSE 8081
ADD target/springdemo.jar springdemo.jar
ENTRYPOINT ["java","-jar","/springdemo.jar"]